
from flask import (
    Flask,
    flash,
    url_for,
    redirect,
    request, 
    render_template
)

app = Flask(__name__)
app.secret_key = 'your_secret_key'

@app.route('/')
def home():
    return render_template('base_layout.html')

@app.route('/user/login', methods=['GET','POST'])
def login():
    if request.method == 'GET':
        return render_template('login.html')

    # POST data
    username = request.form['username']
    privilege = request.form['recommendation']
    password = request.form['password']
    remember = request.form['remember'] if 'remember' in request.form else '0'

    # GET data
    task = request.args.get('task')
    #utm_source = request.args.get('store')
    
    if task=='store':
        task_value = task
        flash('{}, [{}] is {}!'.format(username, privilege, task_value))
        return redirect( url_for('success', task=task_value) )
    elif task=='display':
        task_value = task
        return redirect( url_for('data', username=username,privilege=privilege, password=password, remember=remember, task=task_value))
    elif task==None:
        task_value = 'saved'
        return redirect( url_for('login', username=username, task=task_value) )
    
    # notification
    #flash('{}, [{}] is {}!'.format(username, privilege, task_value))
    #return redirect( url_for('data', username=username, task=task) )

@app.route('/')
def index():
	return render_template('base_layout.html')

@app.route('/success')
def success():
    return render_template('success.html')

@app.route('/data')
def data():
    return render_template('data.html')


if __name__ == "__main__":
	app.run()
