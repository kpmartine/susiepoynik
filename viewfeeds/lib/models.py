from sqlalchemy import Column, DateTime, Integer, String, Text, text
from sqlalchemy.ext.declarative import declarative_base

Base = declarative_base()
metadata = Base.metadata

class Feed(Base):
    __tablename__ = 'feeds'

    id = Column(Integer, primary_key=True)
    user_id = Column(Integer)
    title = Column(String(80), unique=True)
    slug = Column(String(80))
    feed_url = Column(String(255))
    description = Column(Text())
    created_at = Column(DateTime)
    updated_at = Column(DateTime)
    deleted_at = Column(DateTime)
    published = Column(Integer)

toDict = lambda r: {c.name: str(getattr(r, c.name)) for c in r.__table__.columns}

def store_models(engine):
    Base.metadata.create_all(bind=engine)