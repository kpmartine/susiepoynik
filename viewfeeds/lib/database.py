from sqlalchemy import create_engine
from sqlalchemy.orm import scoped_session, sessionmaker
from sqlalchemy.ext.declarative import declarative_base

engine = create_engine('mysql://pyuser:pypass@192.168.11.250/sandbox_eegator', convert_unicode=True)

session_cfg = {
    'autocommit': False,
    'autoflush': False,
    'bind': engine
}
db_session = scoped_session(sessionmaker(**session_cfg))

Base = declarative_base()
Base.query = db_session.query_property()

#
# initialise database
#
def init_db_schema():
    from models import store_models

    store_models(engine)

if __name__ == '__main__':
    init_db_schema()