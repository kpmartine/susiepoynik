from flask import (
    Flask,
    url_for,
    render_template,
    request,
    redirect,
    flash
)
from lib.models import toDict, Feed
from lib.database import db_session

app = Flask(__name__)
app.debug = True

@app.teardown_appcontext
def shutdown_session(exception=None):
    db_session.remove()

@app.route('/')
def home():
    return render_template('base_layout.html')

@app.route('/about')
def about():
    return render_template('about.html')

@app.route('/feeds/')
@app.route('/feeds/<int:uid>')
@app.route('/feeds/<uid>/<task>', methods=['GET','POST'])
def feeds(uid=None, task=None):
    feed = None
    try:
        feed = toDict(db_session.query(Feed).filter(Feed.id==uid).one())
        if task=='update':
            print request.form
            return render_template('update.html', data=request.form)
            
    
        if task=='edit':
            print 'editing'
            return render_template('edit.html', feed=feed)
        else:
            print 'all feeds'
            return render_template('view_feeds.html', feed=feed)

        #return render_template('view_feeds.html', feed=feed)

    except:
        # display all
        feeds = db_session.query(Feed).all()
        return render_template('feeds.html', feeds=feeds)
    
if __name__ == '__main__':
    app.run()