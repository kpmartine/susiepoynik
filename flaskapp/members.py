from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import Column, Integer, String
from sqlalchemy import create_engine


Base = declarative_base()


class Member(Base):
	__tablename__ = 'members'

	id = Column(Integer, primary_key = True)
	first_name = Column(String(255), nullable = False)
	last_name = Column(String(255), nullable = False)
	description = Column(String(255)) 

	def __init__(self, first_name, last_name, description):
		self.first_name = first_name
		self.last_name = last_name
		self.description = description

	def __repr__(self):
		return "{} {}, {}".format(self.first_name, self.last_name, self.description)



engine = create_engine('mysql://nikkimartinez:!nikkimartinez@192.168.11.248/nikkimartinez')
Base.metadata.create_all(engine)
from sqlalchemy.orm import sessionmaker
Session = sessionmaker(bind=engine)
session = Session()
#session.add_all([
#	Member("Philip", "Caingles", "Adult Married Male"),
#	Member("Nikki", "Martinez", "Young Single Female"),
#	Member("Susie", "Maestre", "Young Single Female")
#])
#session.commit()

#members = session.query(Member).all()
#for members in member:
#	print members

